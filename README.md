api-test

Для запуска api сделать клон репозитория

**git clone https://gitlab.com/andrybitko/werush.git**

Проинсталировать композер

**php composer.phar install**

запустить докер

**docker-compose up -d**

Сделать миграции используя внешнее подключение

DATABASE_URL="mysql://root@127.0.0.1:3308/werush?serverVersion=8.0"
потом вернуть обратно
**DATABASE_URL="mysql://root@mysql:3306/werush?serverVersion=8.0"**


Документация

 - localhost:81/api/users - Возвращает список пользователей (Method - GET)**
 - localhost:81/api/users/{id} - Возвращает одного пользователя (Method - GET, {id} - integer)
 - localhost:81/api/users/create - Создает пользователя (Method - POST, parameters: {username: string, email: string, password: string})
 - localhost:81/api/users/update/{id} - Обновляет пользователя (Method - POST, parameters: {username: string, email: string, password: string})
 - localhost:81/api/users/delete/{id} - Удаляет пользователя (Method - DELETE, {id} - integer)
 - localhost:81/api/users/find?username={username} - ищет пользователя по 'username'
 - localhost:81/api/users/find?email={email} - ищет пользователя по 'email'
