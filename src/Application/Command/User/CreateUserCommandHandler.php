<?php

declare(strict_types=1);

namespace App\Application\Command\User;


use App\Entity\User;
use App\Repository\UserRepositoryInterface;

class CreateUserCommandHandler
{
    private UserRepositoryInterface $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function __invoke(CreateUserCommand $createUserCommand)
    {
        $user = new User($createUserCommand->getName(), $createUserCommand->getEmail(), $createUserCommand->getPassword());

        $this->userRepository->add($user);
    }

}
