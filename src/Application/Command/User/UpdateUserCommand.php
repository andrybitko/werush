<?php


namespace App\Application\Command\User;


class UpdateUserCommand
{
    private int $id;

    private ?string $name;

    private ?string $email;

    private ?string $password;

    public function __construct(int $id, ?string $name, ?string $email, ?string $password)
    {
        $this->id = $id;
        $this->name = $name;
        $this->email = $email;
        $this->password = $password;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getId()
    {
        return $this->id;
    }
}
