<?php

declare(strict_types=1);

namespace App\Application\Command\User;


use App\Repository\UserRepositoryInterface;
use Doctrine\ORM\EntityNotFoundException;

class DeleteUserCommandHandler
{
    private UserRepositoryInterface $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function __invoke(int $id)
    {
        $user = $this->userRepository->find($id);

        if ($user === null) {
            throw new EntityNotFoundException(sprintf('User with id "%s" is not found', $id));
        }

        $this->userRepository->remove($user);
    }

}
