<?php

declare(strict_types=1);

namespace App\Application\Command\User;


use App\Entity\User;
use App\Repository\UserRepositoryInterface;
use Doctrine\ORM\EntityNotFoundException;

class UpdateUserCommandHandler
{
    private UserRepositoryInterface $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function __invoke(UpdateUserCommand $updateUserCommand)
    {
        $user = $this->userRepository->find($updateUserCommand->getId());

        if ($user === null) {
            throw new EntityNotFoundException(sprintf('Classroom with id "%s" is not found', $updateUserCommand->getId()));
        }

        if ($updateUserCommand->getName() !== null) {
            $user->setUsername($updateUserCommand->getName());
        }
        if ($updateUserCommand->getEmail() !== null) {
            $user->setEmail($updateUserCommand->getEmail());
        }
        if ($updateUserCommand->getPassword() !== null) {
            $user->setPassword($updateUserCommand->getPassword());
        }
        $this->userRepository->add($user);
    }

}
