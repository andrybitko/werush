<?php

declare(strict_types=1);

namespace App\Application\Query\User;


use App\Application\Query\DTO\UserDTO;
use App\Entity\User;
use App\Repository\UserRepositoryInterface;
use Doctrine\ORM\EntityNotFoundException;

class FindUserQueryHandler
{
    private UserRepositoryInterface $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function __invoke(FindUserQuery $findUserQuery)
    {
        $users = null;
        if ($findUserQuery->getName() !== null && $findUserQuery->getEmail() !== null) {
            $users = $this->userRepository->findBy(['username' => $findUserQuery->getName(), 'email' => $findUserQuery->getEmail()]);
        } else if ($findUserQuery->getName() !== null) {
            $users = $this->userRepository->findBy(['username' => $findUserQuery->getName()]);
        } else if ($findUserQuery->getName() !== null) {
            $users = $this->userRepository->findBy(['email' => $findUserQuery->getEmail()]);
        }

        if ($users === null) {
            throw new EntityNotFoundException(sprintf('User is not found'));
        }

        return $this->fetchUsersDTO($users);
    }

    /**
     * @param User[] $userList
     * @return UserDTO[]
     */
    private function fetchUsersDTO(array $userList): array
    {
        $userDTOList = [];
        foreach ($userList as $user) {
            $userDTO = new UserDTO();
            $userDTO->id = $user->getId();
            $userDTO->name = $user->getUsername();
            $userDTO->email = $user->getEmail();
            $userDTOList[] = $userDTO;
        }

        return $userDTOList;
    }
}