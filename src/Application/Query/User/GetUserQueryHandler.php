<?php

declare(strict_types=1);

namespace App\Application\Query\User;


use App\Application\Query\DTO\UserDTO;
use App\Entity\User;
use App\Repository\UserRepositoryInterface;
use Doctrine\ORM\EntityNotFoundException;

class GetUserQueryHandler
{
    private UserRepositoryInterface $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function __invoke(int $id)
    {
        $user = $this->userRepository->find($id);

        if ($user === null) {
            throw new EntityNotFoundException(sprintf('User with id "%s" is not found', $id));
        }

        return $this->fetchUserDTO($user);
    }

    private function fetchUserDTO(User $user): UserDTO
    {
        $userDTO = new UserDTO();
        $userDTO->id = $user->getId();
        $userDTO->name = $user->getUsername();
        $userDTO->email = $user->getEmail();

        return $userDTO;
    }
}