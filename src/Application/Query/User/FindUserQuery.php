<?php


namespace App\Application\Query\User;


class FindUserQuery
{
    private ?string $name = null;

    private ?string $email = null;

    public function __construct(?string $name, ?string $email)
    {
        $this->name = $name;
        $this->email = $email;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getEmail()
    {
        return $this->email;
    }
}
