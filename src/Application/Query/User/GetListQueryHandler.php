<?php

declare(strict_types=1);

namespace App\Application\Query\User;


use App\Application\Query\DTO\UserDTO;
use App\Entity\User;
use App\Repository\UserRepositoryInterface;

class GetListQueryHandler
{
    private UserRepositoryInterface $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function __invoke()
    {
        $userList = $this->userRepository->findAll();

        return $userList !== [] ? $this->fetchUsersDTO($userList) : [];
    }

    /**
     * @param User[] $userList
     * @return UserDTO[]
     */
    private function fetchUsersDTO(array $userList): array
    {
        $userDTOList = [];
        foreach ($userList as $user) {
            $userDTO = new UserDTO();
            $userDTO->id = $user->getId();
            $userDTO->name = $user->getUsername();
            $userDTO->email = $user->getEmail();
            $userDTOList[] = $userDTO;
        }

        return $userDTOList;
    }
}