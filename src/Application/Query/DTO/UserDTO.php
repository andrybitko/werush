<?php


namespace App\Application\Query\DTO;


class UserDTO
{
    public int $id;

    public string $name;

    public string $email;
}