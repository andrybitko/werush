<?php

declare(strict_types=1);

namespace App\Repository;


use App\Entity\User;

Interface UserRepositoryInterface
{
    /**
     * @param int $id
     * @param null $lockMode
     * @param null $lockVersion
     * @return User
     */
    public function find($id, $lockMode = null, $lockVersion = null);
    /**
     * @return User[]
     */
    public function findAll();

    public function add(User $classroom);

    public function remove(User $classroom);

    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null);
}
