<?php

declare(strict_types=1);

namespace App\Controller\User;


use App\Application\Query\User\GetUserQueryHandler;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class GetUserController extends AbstractController
{
    private GetUserQueryHandler $getUserQueryHandler;

    public function __construct(GetUserQueryHandler $getUserQueryHandler)
    {
        $this->getUserQueryHandler = $getUserQueryHandler;
    }

    /**
     * @Route("/api/users/{id}", name="user_id", methods={"GET"})
     * @param int $id
     * @return JsonResponse
     */
    public function __invoke(int $id)
    {
        return new JsonResponse(($this->getUserQueryHandler)($id), Response::HTTP_OK);
    }
}