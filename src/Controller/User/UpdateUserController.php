<?php

declare(strict_types=1);

namespace App\Controller\User;


use App\Application\Command\User\UpdateUserCommand;
use App\Application\Command\User\UpdateUserCommandHandler;
use App\Controller\RequestDTO\Api\UpdateUserDTO;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UpdateUserController extends AbstractController
{
    private UpdateUserCommandHandler $updateUserQueryHandler;

    private ValidatorInterface $validator;

    public function __construct(
        UpdateUserCommandHandler $updateUserQueryHandler,
        ValidatorInterface $validator
        )
    {
        $this->updateUserQueryHandler = $updateUserQueryHandler;
        $this->validator = $validator;
    }

    /**
     * @Route("/api/users/update/{id}", name="user_update", methods={"POST"})
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function __invoke(Request $request, int $id)
    {
        $updateUserDTO = $this->prepareUpdateUserDTO($request, $id);

        $errors = $this->validator->validate($updateUserDTO);

        if (count($errors) > 0) {
            $errorsString = (string)$errors;

            return new JsonResponse(['error' => $errorsString], Response::HTTP_BAD_REQUEST);
        }

        $updateUserCommand = new UpdateUserCommand(
            $id,
            $updateUserDTO->name,
            $updateUserDTO->email,
            password_hash($updateUserDTO->password, PASSWORD_DEFAULT)
        );

        return new JsonResponse(($this->updateUserQueryHandler)($updateUserCommand), Response::HTTP_OK);
    }

    public function prepareUpdateUserDTO(Request $request, $id): UpdateUserDTO
    {
        $updateUserDTO = new UpdateUserDTO();
        $updateUserDTO->id = $id;
        $updateUserDTO->name = $request->request->get('name');
        $updateUserDTO->email = $request->request->get('email');
        $updateUserDTO->password = $request->request->get('password');

        return $updateUserDTO;
    }
}
