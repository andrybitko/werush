<?php

declare(strict_types=1);

namespace App\Controller\User;


use App\Application\Query\User\GetListQueryHandler;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class GetListController extends AbstractController
{
    private GetListQueryHandler $getListQueryHandler;

    public function __construct(GetListQueryHandler $getListQueryHandler)
    {
        $this->getListQueryHandler = $getListQueryHandler;
    }
    /**
     * @Route("/api/users", name="user_list", methods={"GET"})
     */
    public function __invoke()
    {
        return new JsonResponse(($this->getListQueryHandler)(), Response::HTTP_OK);
    }
}