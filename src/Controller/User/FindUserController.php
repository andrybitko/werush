<?php

declare(strict_types=1);

namespace App\Controller\User;


use App\Application\Query\User\FindUserQuery;
use App\Application\Query\User\FindUserQueryHandler;
use App\Controller\RequestDTO\Api\FindUserDTO;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class FindUserController extends AbstractController
{
    private FindUserQueryHandler $findUserQueryHandler;

    public function __construct(FindUserQueryHandler $findUserQueryHandler)
    {
        $this->findUserQueryHandler = $findUserQueryHandler;
    }

    /**
     * @Route("/api/users/find", name="user_find", methods={"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function __invoke(Request $request)
    {
        $findUserDTO = $this->prepareFindUserDTO($request);

        $findUserQuery = new FindUserQuery($findUserDTO->name, $findUserDTO->email);

        return new JsonResponse(($this->findUserQueryHandler)($findUserQuery), Response::HTTP_OK);
    }

    public function prepareFindUserDTO(Request $request): FindUserDTO
    {

        $findUserDTO = new FindUserDTO();
        $findUserDTO->name = $request->get('username');
        $findUserDTO->email = $request->get('email');

        return $findUserDTO;
    }
}