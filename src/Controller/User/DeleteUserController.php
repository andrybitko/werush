<?php

declare(strict_types=1);

namespace App\Controller\User;


use App\Application\Command\User\DeleteUserCommandHandler;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class DeleteUserController extends AbstractController
{
    private DeleteUserCommandHandler $deleteUserCommandHandler;

    public function __construct(DeleteUserCommandHandler $deleteUserCommandHandler)
    {
        $this->deleteUserCommandHandler = $deleteUserCommandHandler;
    }

    /**
     * @Route("/api/users/delete/{id}", name="user_delete", methods={"DELETE"})
     * @param int $id
     * @return JsonResponse
     */
    public function __invoke(int $id)
    {
        return new JsonResponse(($this->deleteUserCommandHandler)($id), Response::HTTP_OK);
    }
}
