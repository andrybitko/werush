<?php

declare(strict_types=1);

namespace App\Controller\User;


use App\Application\Command\User\CreateUserCommand;
use App\Application\Command\User\CreateUserCommandHandler;
use App\Controller\RequestDTO\Api\CreateUserDTO;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class CreateUserController extends AbstractController
{
    private CreateUserCommandHandler $createUserQueryHandler;

    private ValidatorInterface $validator;

    public function __construct(
        CreateUserCommandHandler $createUserCommandHandler,
        ValidatorInterface $validator
    )
    {
        $this->createUserQueryHandler = $createUserCommandHandler;
        $this->validator = $validator;
    }

    /**
     * @Route("/api/users/create", name="user_create", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function __invoke(Request $request)
    {
        $createUserDTO = $this->prepareCreateUserDTO($request);

        $errors = $this->validator->validate($createUserDTO);

        if (count($errors) > 0) {
            $errorsString = (string)$errors;

            return new JsonResponse(['error' => $errorsString], Response::HTTP_BAD_REQUEST);
        }

        $createUserCommand = new CreateUserCommand(
            $createUserDTO->name,
            $createUserDTO->email,
            password_hash($createUserDTO->password, PASSWORD_DEFAULT)
        );

        return new JsonResponse(($this->createUserQueryHandler)($createUserCommand), Response::HTTP_OK);
    }

    public function prepareCreateUserDTO(Request $request): CreateUserDTO
    {
        $createUserDTO = new CreateUserDTO();
        $createUserDTO->name = $request->request->get('name');
        $createUserDTO->email = $request->request->get('email');
        $createUserDTO->password = $request->request->get('password');

        return $createUserDTO;
    }
}
