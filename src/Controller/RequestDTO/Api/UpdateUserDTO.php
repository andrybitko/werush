<?php


namespace App\Controller\RequestDTO\Api;


class UpdateUserDTO
{
    public int $id;

    public ?string $name = null;

    public ?string $email = null;

    public ?string $password = null;
}
