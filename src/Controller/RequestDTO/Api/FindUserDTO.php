<?php


namespace App\Controller\RequestDTO\Api;


class FindUserDTO
{
    public ?string $name = null;

    public ?string $email = null;
}
