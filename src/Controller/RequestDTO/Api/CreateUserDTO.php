<?php


namespace App\Controller\RequestDTO\Api;


class CreateUserDTO
{
    public ?string $name = null;

    public ?string $email = null;

    public ?string $password = null;
}
